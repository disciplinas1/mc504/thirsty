#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>
#include "libtst/barrier.h"
#include "libtst/semaphore.h"

#define MAX_THREADS 1000
#define MIN_GLASS_HEIGHT 3
#define MAX_GLASS_HEIGHT 8
#define GLASS_WIDTH 3
#define H_SYMBOL "H"
#define O_SYMBOL "O"
#define WATER_SYMBOL "H2O"

// Global Variables:
int o_count;
int h_count;
int water_count;
int glass_height;

// Semaphores and Barrier to solve:
tst_sem_t *mutex;
tst_sem_t *o_queue;
tst_sem_t *h_queue;
tst_barrier_t *barrier;

/**
 * Print a string multiple times in a line.
 *
 * @param symbol String to be printed.
 * @param sep Separator between symbols.
 * @param times Number of times to print.
 */
void print_multiple(const char *symbol, const char *sep, int times) {
    for (int i = 0; i < times; i++) {
        printf("%s", symbol);

        if (i != times - 1) {
            printf("%s", sep);
        }
    }

    printf("\n");
}

/**
 * Print a separator.
 *
 * @param size Separator size.
 */
void print_separator(int size) {
    print_multiple("=", "=", size);
}

/**
 * Print oxygen and hydrogen queues.
 */
void print_queues() {
    int sep_width = 10;

    printf("\n");
    printf("Hydrogen queue:\n");
    print_separator(sep_width);
    print_multiple(H_SYMBOL, " ", h_count);
    print_separator(sep_width);
    printf("\n");
    printf("Oxygen queue:\n");
    print_separator(sep_width);
    print_multiple(O_SYMBOL, " ", o_count);
    print_separator(sep_width);
    printf("\n");
}

/**
 * Print glass of water with current number of molecules.
 */
void print_glass() {
    int col_width = strlen(WATER_SYMBOL);
    int cells_to_print = glass_height * GLASS_WIDTH;
    char space[col_width];
    sprintf(space, "%*s", col_width, "");

    printf("Your Glass:\n\n");
    for (int row = 0; row < glass_height; row++) {
        printf("|");

        for (int col = 0; col < GLASS_WIDTH; col++) {
            if (cells_to_print > water_count) {
                printf("%s", space);
            } else {
                printf("%s", WATER_SYMBOL);
            }

            if (col != GLASS_WIDTH - 1) {
                printf(" ");
            }

            cells_to_print--;
        }

        printf("|\n");
    }

    printf("+");
    for (int i = 0; i < (col_width + 1) * GLASS_WIDTH - 1; i++) {
        printf("-");
    }

    printf("+\n");
}

/**
 * Print queues and glass and then post the mutex semaphore.
 */
void print_system_and_post_mutex() {
    print_queues();
    print_glass();
    tst_sem_post(mutex);
}

/**
 * Represent the oxygen behavior in order to solve the problem.
 */
void *o_thread(void *args) {
    tst_sem_wait(mutex);

    o_count += 1;
    if (h_count >= 2) {
        tst_sem_post_multiple(h_queue, 2);
        h_count -= 2;
        tst_sem_post(o_queue);
        o_count -= 1;
    } else {
        print_system_and_post_mutex();
    }

    tst_sem_wait(o_queue);
    tst_barr_wait(barrier);

    water_count += 1;
    print_system_and_post_mutex();

    return NULL;
}

/**
 * Represent the hydrogen behavior in order to solve the problem.
 */
void *h_thread(void *args) {
    tst_sem_wait(mutex);

    h_count += 1;
    if (h_count >= 2 && o_count >= 1) {
        tst_sem_post_multiple(h_queue, 2);
        h_count -= 2;
        tst_sem_post(o_queue);
        o_count -= 1;
    } else {
        print_system_and_post_mutex();
    }

    tst_sem_wait(h_queue);
    tst_barr_wait(barrier);

    return NULL;
}

/**
 * Prompts user for a height for the glass.
 * The value must be in the constraints defined by the glass height constants.
 *
 * @return Glass height.
 */
int get_glass_height() {
    int height = 0;
    bool did_choose = false;

    printf("Choose your glass height (from %d to %d):\n", MIN_GLASS_HEIGHT, MAX_GLASS_HEIGHT);
    while (!did_choose) {
        scanf("%d", &height);

        if (height > MAX_GLASS_HEIGHT) {
            printf("Too large! Choose again:");
        } else if (height < MIN_GLASS_HEIGHT) {
            printf("That's too small! Another size:");
        } else {
            did_choose = true;
        }
    }

    return height;
}

int main(void) {
    mutex = tst_sem_open("mutex", 1);
    o_queue = tst_sem_open("o_queue", 0);
    h_queue = tst_sem_open("h_queue", 0);
    barrier = tst_barr_open("barrier", 3);
    o_count = 0;
    h_count = 0;
    glass_height = get_glass_height();

    printf("To call for a random new atom, press ENTER\n");
    getchar();

    pthread_t o_threads[MAX_THREADS];
    pthread_t h_threads[MAX_THREADS];
    int o_thread_count = 0;
    int h_thread_count = 0;

    while (water_count < glass_height * GLASS_WIDTH) {
        getchar();
        int choice = rand() % 3;

        if (choice <= 1) {
            pthread_create(&h_threads[h_thread_count], NULL, h_thread, NULL);
            h_thread_count++;
        } else {
            pthread_create(&o_threads[o_thread_count], NULL, o_thread, NULL);
            o_thread_count++;
        }
    }

    for (int i = 0; i < o_thread_count; i++) {
        pthread_cancel(o_threads[i]);
    }

    for (int i = 0; i < h_thread_count; i++) {
        pthread_cancel(h_threads[i]);
    }

    getchar();
    printf("Now that's a full glass!! Aaah\n");

    tst_sem_close(mutex);
    tst_sem_close(o_queue);
    tst_sem_close(h_queue);
    tst_barr_close(barrier);

    return EXIT_SUCCESS;
}
