CFLAGS ?=  -pthread -Wall -Werror -Wpedantic -Wunused-result

PROGRAMS = thirsty

all: $(PROGRAMS)

thirsty: thirsty.o libtst/semaphore.o libtst/barrier.o
	gcc $(CFLAGS) $^ -o $@

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o libtst/*.o $(PROGRAMS)
