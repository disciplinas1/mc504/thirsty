#ifndef __TST_SEMAPHORE_H__
#define __TST_SEMAPHORE_H__

#include <semaphore.h>

/* Semaphore type. */
typedef sem_t tst_sem_t;

/**
 * Unlink any semaphore linked to 'name' and open a new semaphore.
 *
 * @param name Semaphore name.
 * @param value Initial value.
 * @return Pointer to the semaphore.
 */
tst_sem_t *tst_sem_open(const char *name, int value);

/**
 * Wait on a semaphore.
 *
 * @param sem Pointer to the semaphore.
 * @return If sucessful, return EXIT_SUCESS. Otherwise, EXIT_FAILURE is returned.
 */
int tst_sem_wait(tst_sem_t *sem);

/**
 * Post (signal) to a semaphore.
 *
 * @param sem Pointer to the semaphore.
 * @return If sucessful, return EXIT_SUCESS. Otherwise, EXIT_FAILURE is returned.
 */
int tst_sem_post(tst_sem_t *sem);

/**
 * Post (signal) multiple times to a semaphore.
 *
 * @param sem Pointer to the semaphore.
 * @param times Number of times to post.
 * @return If sucessful, return EXIT_SUCESS. Otherwise, EXIT_FAILURE is returned.
 */
int tst_sem_post_multiple(tst_sem_t *sem, int times);

/**
 * Close a semaphore.
 *
 * @param sem Pointer to the semaphore.
 * @return If sucessful, return EXIT_SUCESS. Otherwise, EXIT_FAILURE is returned.
 */
int tst_sem_close(tst_sem_t *sem);


#endif // __TST_SEMAPHORE_H__
