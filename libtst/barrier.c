#include "barrier.h"
#include "semaphore.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct tst_barrier {
    int size;
    int count;
    tst_sem_t *mutex;
    tst_sem_t *turnstile1;
    tst_sem_t *turnstile2;
};

tst_barrier_t *tst_barr_open(const char *name, int size) {
    tst_barrier_t *barr = malloc(sizeof(struct tst_barrier));
    if (barr == NULL) {
        printf("Error: failed to allocate memory for barrier\n");
        return NULL;
    }

    size_t sem_name_len = strlen(name) + strlen("/turnstile1");
    char *sem_name = malloc((sem_name_len + 1) * sizeof(char));

    barr->size = size;
    barr->count = 0;

    sprintf(sem_name, "%s/mutex", name);
    barr->mutex = tst_sem_open(sem_name, 1);
    sprintf(sem_name, "%s/turnstile1", name);
    barr->turnstile1 = tst_sem_open(sem_name, 0);
    sprintf(sem_name, "%s/turnstile2", name);
    barr->turnstile2 = tst_sem_open(sem_name, 1);

    free(sem_name);
    return barr;
}

int tst_barr_phase1(tst_barrier_t *barr) {
    tst_sem_wait(barr->mutex);

    barr->count += 1;
    if (barr->count == barr->size) {
        tst_sem_wait(barr->turnstile2);
        tst_sem_post(barr->turnstile1);
    }

    tst_sem_post(barr->mutex);

    tst_sem_wait(barr->turnstile1);
    tst_sem_post(barr->turnstile1);

    return EXIT_SUCCESS;
}

int tst_barr_phase2(tst_barrier_t *barr) {

    tst_sem_wait(barr->mutex);

    barr->count -= 1;
    if (barr->count == 0) {
        tst_sem_wait(barr->turnstile1);
        tst_sem_post(barr->turnstile2);
    }

    tst_sem_post(barr->mutex);

    tst_sem_wait(barr->turnstile2);
    tst_sem_post(barr->turnstile2);

    return EXIT_SUCCESS;
}

int tst_barr_wait(tst_barrier_t *barr) {
    tst_barr_phase1(barr);
    tst_barr_phase2(barr);

    return EXIT_SUCCESS;
}

int tst_barr_close(tst_barrier_t *barr) {
    tst_sem_close(barr->mutex);
    tst_sem_close(barr->turnstile1);
    tst_sem_close(barr->turnstile2);

    return EXIT_SUCCESS;
}
