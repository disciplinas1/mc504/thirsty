#include "semaphore.h"
#include <stdlib.h>
#include <fcntl.h>

tst_sem_t *tst_sem_open(const char *name, int value) {
    sem_unlink(name);
    return sem_open(name, O_CREAT, S_IRWXU, value);
}

int tst_sem_wait(tst_sem_t *sem) {
    return sem_wait(sem);
}

int tst_sem_post(tst_sem_t *sem) {
    return sem_post(sem);
}

int tst_sem_post_multiple(tst_sem_t *sem, int times) {
    int ret;
    for (int i = 0; i < times; i++) {
        ret = tst_sem_post(sem);
        if (ret != EXIT_SUCCESS) {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int tst_sem_close(tst_sem_t *sem) {
    return sem_close(sem);
}
