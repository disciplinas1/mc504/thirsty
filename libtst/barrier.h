#ifndef __TST_BARRIER_H__
#define __TST_BARRIER_H__

/* Barrier type. */
typedef struct tst_barrier tst_barrier_t;

/**
 * Open a barrier.
 *
 * @param name Barrier name.
 * @return Pointer to the barrier.
 */
tst_barrier_t *tst_barr_open(const char *name, int size);

/**
 * Wait in a barrier.
 *
 * @param tst_barr Pointer to the barrier.
 * @return If sucessful, return EXIT_SUCESS. Otherwise, EXIT_FAILURE is returned.
 */
int tst_barr_wait(tst_barrier_t *barr);

/**
 * Close a barrier.
 *
 * @param barr Pointer to the barrier.
 * @return If sucessful, return EXIT_SUCESS. Otherwise, EXIT_FAILURE is returned.
 */
int tst_barr_close(tst_barrier_t *barr);

#endif // __TST_BARRIER_H__
