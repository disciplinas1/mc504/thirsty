# Thirsty

This project was inspired in the "Building H<sub>2</sub>O" problem from the [Little Book of Semaphores](https://greenteapress.com/wp/semaphores/).

To run:

    make
    ./thirsty

## Problem

The problem consists in using threads to represent hydrogen and oxygen atoms that arrive in some order and using them to build water molecules. To do that, it is necessary to hold atoms until there are 2 hydrogens and 1 oxygen available.

## Solution

The solution uses three independent semaphores and a barrier. The barrier requires 3 threads to open the turnstile (3 atoms) and the semaphores are used to represent a hydrogen queue, an oxygen queue and a mutex to keep the queues safe.

Each new atom (new thread) waits for the mutex semaphore. Once it gets access, it increases the counter of its type and check if the queues have enough atoms for a molecule. If they don't, the mutex is posted and the atom wait on its queue. If they do, the queues receive the appropriate number of posts (2 for hydrogen and 1 for oxygen).

## Implementation

For the semaphore structure, although implemented in the standard library, an interface was built to ease compatibility with different operating systems. A reusable barrier type was implemented with its own interface as well. Both of this structure are in the separated *libtst*.

During the execution, the whole state of the process is printed at each step rather than a simple log of the events. To achieve this, every post done to the mutex semaphore is preceded with a print. Therefore, there's never two system prints competing (safe by mutex) and every new thread causes a new print.

For the problem itself, all of the implementation is in *thirsty.c*. This includes the threads behavior (solution of the problem) and the interaction with the user (printing and prompting for actions).
